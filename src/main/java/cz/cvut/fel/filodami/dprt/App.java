package cz.cvut.fel.filodami.dprt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class App {

	private static final Logger log = LoggerFactory.getLogger(App.class);
	private static final File CONFIG_FILE = new File("cfg.json");
	private static final Scanner scn = new Scanner(System.in);
	private static final GsonBuilder gb = new GsonBuilder();
	private static int SERVER_PORT = 3030;
	private static Map<String, Dictionary> dictionaries = new HashMap<>();
	private static AppState state = AppState.CONFIG;
	private static Config config;
	private static String[] input;
	private static Server server;

	private static Executor exe;

	public static void main(String[] args) {
		if (args.length > 0) {
			try {
				SERVER_PORT = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("Unable to parse port number, using default (3030).");
			}
		}

		gb.setPrettyPrinting();
		try {
			config = loadConfig(CONFIG_FILE);
		} catch (IOException e) {
			System.err.println("Unable to load or create default config file!");
			e.printStackTrace();
			return;
		}
		reloadDictionaries();
		boolean repeat = true;
		while (repeat) {
			System.out.print("> ");
			input = scn.nextLine().split(" ");
			if (input.length == 0)
				continue;
			switch (input[0]) {
			case "solve":
				if (input.length == 1) {
					if (state == AppState.READY)
						solve();
					else
						System.err.println("For Job assignment this node needs to be in READY state.");
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "kill":
				if (input.length == 2) {
					if (state == AppState.READY)
						killJob(input[1]);
					else
						System.err.println("For Job kill this node needs to be in READY state.");
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "check":
				if (input.length == 3) {
					if (state == AppState.READY) {
						try {
							String[] ip = input[2].split(":");
							if (ip.length == 2)
								Config.validateAddress(ip[0], ip[1]);
							else
								throw new InvalidParameterException("Address must be in format <ipv4>:<port>");

							if (Overwatch.getInstance().isTerminated(input[1], input[2])) {
								log.info("Job " + input[1] + " is terminated.");
							} else {
								log.info("Job " + input[1] + " is not yet terminated.");
							}
						} catch (InvalidParameterException e) {
							System.err.println("Invalid parameter! " + e.getMessage());
						}
					} else
						System.err.println("For Job termination check this node needs to be in READY state.");
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "show":
				if (input.length == 2) {
					show(input[1]);
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "config":
				if (input.length == 1) {
					if (state == AppState.CONFIG)
						config();
					else
						System.err.println("For configuration change this node needs to be in CONFIG state.");
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "start":
				if (input.length == 1) {
					initialize();
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "stop":
				if (input.length == 1) {
					resetNode();
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "help":
				if (input.length == 1) {
					printHelp();
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			case "exit":
				if (input.length == 1) {
					repeat = false;
				} else {
					System.err.println("Invalid syntax!");
					printHelp();
				}
				break;
			default:
				if (!input[0].isEmpty()) {
					System.err.println("Invalid command!");
					printHelp();
				}
				break;
			}
		}
		resetNode();
	}

	private static void solve() {
		boolean repeat = true;
		JobBuilder jobb = new JobBuilder("MD5", "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", "",
				"", 6);
		int splitOrder = 7;
		while (repeat) {
			System.out.print("solve> ");
			input = scn.nextLine().split(" ");
			if (input.length == 0)
				return;
			switch (input[0]) {
			case "set":
				if (input.length == 3) {
					try {
						switch (input[1]) {
						case "algorithm":
							jobb.setAlgorithm(input[2]);
							break;
						case "alphabet":
							jobb.setAlphabet(input[2]);
							break;
						case "saltsize":
							jobb.setSaltSize(input[2]);
							break;
						case "dictionary":
							jobb.setDictionary(input[2]);
							break;
						case "ciphertext":
							jobb.setCiphertext(input[2]);
							break;
						case "split":
							try {
								int s = Integer.parseInt(input[2]);
								if (s >= 0)
									splitOrder = s;
								else
									throw new InvalidParameterException("Order of Job split cannot be less than 0!");
							} catch (NumberFormatException e) {
								throw new InvalidParameterException("Unable to parse order of split.");
							}
							break;
						default:
							System.err.println("Invalid syntax!");
							printSolveHelp();
							break;
						}
					} catch (InvalidParameterException e) {
						System.err.println("Invalid parameter! " + e.getMessage());
					}
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			case "show":
				if (input.length == 1) {
					System.out.println(jobb);
					System.out.println("Split Job to: " + Math.pow(2, splitOrder) + " Chunks.");
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			case "ok":
				if (input.length == 1) {
					if (jobb.isValid()) {
						repeat = false;
						exe.registerNewJob(jobb.build(), splitOrder);
					} else {
						System.err.println("Configured Job is not yet valid.");
					}
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			case "cancel":
				if (input.length == 1) {
					repeat = false;
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			case "help":
				if (input.length == 1) {
					printSolveHelp();
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			default:
				if (!input[0].isEmpty()) {
					System.err.println("Invalid command!");
					printSolveHelp();
				}
				break;
			}
		}
	}

	private static Config loadConfig(File file) throws IOException {
		Config cfg;
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			Gson gson = gb.create();
			cfg = gson.fromJson(bufferedReader, Config.class);
			bufferedReader.close();
			System.out.println("Config loaded!");
		} catch (FileNotFoundException | JsonSyntaxException e) {
			cfg = Config.defaultConfig();
			saveConfig(file, cfg);
			System.out.println("Default config created!");
		}
		return cfg;
	}

	private static void saveConfig(File file, Config cfg) throws IOException {
		Gson gson = gb.create();
		file.delete();
		file.createNewFile();
		FileWriter fw = new FileWriter(file);
		fw.write(gson.toJson(cfg));
		fw.close();
	}

	private static void config() {
		boolean repeat = true;
		while (repeat) {
			System.out.print("config> ");
			input = scn.nextLine().split(" ");
			if (input.length == 0)
				return;
			switch (input[0]) {
			case "set":
				if (input.length == 3) {
					try {
						switch (input[1]) {
						case "name":
							config.setNodeName(input[2]);
							break;
						case "threads":
							config.setThreads(input[2]);
							break;
						case "ecsi":
							config.setEcsInterval(input[2]);
							break;
						case "eccs":
							config.setEccSize(input[2]);
							break;
						default:
							System.err.println("Invalid syntax!");
							printConfigHelp();
							break;
						}
					} catch (InvalidParameterException e) {
						System.err.println("Invalid parameter! " + e.getMessage());
					}
				} else {
					System.err.println("Invalid syntax!");
					printSolveHelp();
				}
				break;
			case "add":
				if (input.length == 3) {
					try {
						switch (input[1]) {
						case "neighbor":
							String[] ip = input[2].split(":");
							if (ip.length == 2)
								config.addNeighbor(ip[0], ip[1]);
							else
								System.err.println("Valid ip sysntax is only [ipv4]:[port]");
							break;
						case "dictionary":
							config.addDictionary(input[2]);
							break;
						default:
							System.err.println("Invalid syntax!");
							printConfigHelp();
							break;
						}
					} catch (InvalidParameterException e) {
						System.err.println("Invalid parameter! " + e.getMessage());
					}
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "rem":
				if (input.length == 3) {
					try {
						switch (input[1]) {
						case "neighbor":
							config.removeNeighbor(input[2]);
							break;
						case "dictionary":
							config.removeDictionary(input[2]);
							break;
						default:
							System.err.println("Invalid syntax!");
							printConfigHelp();
							break;
						}
					} catch (InvalidParameterException e) {
						System.err.println("Invalid parameter! " + e.getMessage());
					}
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "show":
				if (input.length == 1) {
					System.out.println(config);
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "ok":
				if (input.length == 1) {
					repeat = false;
					try {
						saveConfig(CONFIG_FILE, config);
						reloadDictionaries();
					} catch (IOException e) {
						System.err.println("Unable to save config file! Config not saved.");
					}
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "cancel":
				if (input.length == 1) {
					repeat = false;
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "reset":
				if (input.length == 1) {
					config = Config.defaultConfig();
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			case "help":
				if (input.length == 1) {
					printConfigHelp();
				} else {
					System.err.println("Invalid syntax!");
					printConfigHelp();
				}
				break;
			default:
				if (!input[0].isEmpty()) {
					System.err.println("Invalid command!");
					printConfigHelp();
				}
				break;
			}
		}
	}

	private static void initialize() {
		exe = Executor.getInstance();
		Overwatch.getInstance();
		reloadDictionaries();
		server = ServerBuilder.forPort(SERVER_PORT).addService(new DPRTJobServer()).addService(new DPRTManagerServer()).build();
		try {
			server.start();
			System.out.println("Server started on port " + SERVER_PORT);
		} catch (IOException e1) {
			System.err.println("Could not start a server on port " + SERVER_PORT);
			// e1.printStackTrace();
			return;
		}
		state = AppState.READY;
	}

	private static void killJob(String jid) {
		if (exe.killJob(jid, false)) {
			exe.getClient().kill(jid);
			System.out.println("Job " + jid + " killed.");
		} else {
			System.err.println("Job " + jid + " not killed. (not active or not present)");
		}
	}

	public static void resetNode() {
		Executor.clear();
		Overwatch.clear();
		exe = null;
		if (server != null) {
			try {
				server.shutdown();
				server.awaitTermination();
				server = null;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		state = AppState.CONFIG;
	}

	private static void reloadDictionaries() {
		dictionaries = new HashMap<>();
		for (String string : config.getDictionaries()) {
			try {
				dictionaries.put(string, Dictionary.loadDictionary(string));
			} catch (IOException e) {
				log.error("Could not load dictionary with name: " + string);
			}
		}
	}

	private static void show(String category) {
		switch (category) {
		case "jobs":
			if (exe != null) {
				System.out.println("Active:");
				System.out.println(exe.getActiveJobs());
				System.out.println("Killed:");
				System.out.println(exe.getKilledJobs());
			}
			break;
		case "results":
			if (exe != null)
				System.out.println(exe.getResults());
			break;
		case "config":
			System.out.println(config);
			break;
		case "core":
			if (exe != null)
				System.out.println(exe);
			break;
		default:
			System.out.println("Not yet implemented.");
			break;
		}
	}

	private static void printHelp() {
		System.out.println(
				"\nAvailable commands:\n solve | Creation process of a new job.\n kill <JobID> | Kill a job in an execution.\n check <jobUUID> <thisNodeAddress>:<port> | Using reliable termination detection algorithm, checks whether Job has been fully terminated. \n show <\"jobs/core/results/config\"> | Show detail about certain part of system.\n config | Configure local node.\n start | Activate execution and job search capabilities of the node. (Change to READY state)\n stop | Reset node and allow configuration. (Change to CONFIG state)\n help\n exit\n");
	}

	private static void printSolveHelp() {
		System.out.println(
				"\nAvailable SOLVE commands:\n set <\"saltsize/alphabet/ciphertext/algorithm/dictionary\"> <value>\n  [algorithm] : MD5 (default), SHA1\n  [dictionary] : john (default)\n show | Show new Job configuration.\n help\n ok | Execute newly configured Job.\n cancel | Discard changes and return to menu.\n");
	}

	private static void printConfigHelp() {
		System.out.println(
				"\nAvailable CONFIG commands:\n set <\"name/threads/ecsi/eccs\"> <value>\n add <\"neighbor/dictionary\"> <index>\n rem <\"neighbor/dictionary\"> <IPv4>\n show | Show current configuration.\n help\n ok | Save configuration and return to menu.\n cancel | Discard changes and return to menu.\n reset | Load default config.\n");
	}

	public static Config getConfig() {
		return config;
	}

	public static Map<String, Dictionary> getDictionaries() {
		return dictionaries;
	}

	public static AppState getState() {
		return state;
	}

}
