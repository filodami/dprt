package cz.cvut.fel.filodami.dprt;

import java.util.ArrayList;
import java.util.List;

public class Range {
	public final Integer[] from;
	public final Integer[] to;
	public final int max;
	public final int mid;

	public Range(Integer[] from, Integer[] to, int max) {
		super();
		this.from = from;
		this.to = to;
		this.max = max;
		this.mid = max / 2;
	}
	
	public static List<Range> splitRanges(List<Range> toSplit) throws NoSplitException {
		List<Range> ret = new ArrayList<>(toSplit.size()*2);
		for (Range range : toSplit) {
			ret.addAll(range.split());
		}
		return ret;
	}

	public List<Range> split() throws NoSplitException {
		List<Range> ret = new ArrayList<>(2);
		Integer[] tmpFrom = new Integer[from.length];
		Integer[] tmpTo = new Integer[to.length];
		for (int j = 0; j < from.length; j++) {
			tmpFrom[j] = 0;
			tmpTo[j] = max;
		}
		for (int i = 0; i < from.length; i++) {
			int diff = (to[i] - from[i]) + 1;
			if (diff > 1) {
				if ((diff % 2) == 1) {
					tmpTo[i] = from[i] + (diff / 2);
					tmpFrom[i] = tmpTo[i];
					if (from.length > i+1) {
						tmpTo[i + 1] = mid;
						tmpFrom[i + 1] = mid + 1;
					} else {
						tmpFrom[i]--;
					}
				} else {
					tmpTo[i] = from[i] + ((diff / 2) - 1);
					tmpFrom[i] = tmpTo[i] + 1;
				}
				ret.add(new Range(from, tmpTo, max));
				ret.add(new Range(tmpFrom, to, max));
				break;
			} else {
				tmpFrom[i] = from[i];
				tmpTo[i] = to[i];
			}
		}
		if (ret.isEmpty())
			throw new NoSplitException();
		return ret;
	}
}
