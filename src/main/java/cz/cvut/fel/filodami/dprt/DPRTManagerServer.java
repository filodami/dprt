package cz.cvut.fel.filodami.dprt;

import java.util.Set;

import cz.cvut.fel.filodami.dprt.DPRTManagerGrpc.DPRTManagerImplBase;
import io.grpc.stub.StreamObserver;

public class DPRTManagerServer extends DPRTManagerImplBase {

	@Override
	public void submitTD(MisraToken request, StreamObserver<MisraToken> responseObserver) {
		MisraToken mt = request;
		String sender = request.getLogList().get(request.getLogCount() - 2);
		if (!Overwatch.getInstance().wasVisited(request.getUuid())) {
			Set<String> neigh = Overwatch.getInstance().getClient().getNeighbors();
			neigh.remove(sender);
			for (String n : neigh) {
				int nb = Executor.getInstance().getActiveJobIds().contains(mt.getJobUuid()) ? 0 : (mt.getNb() + 1);
				MisraToken tmp = Overwatch.getInstance().getClient().sendToken(n,
						MisraToken.newBuilder().setUuid(mt.getUuid()).setJobUuid(mt.getJobUuid()).setNb(nb)
								.addAllLog(mt.getLogList()).addLog(n).build());
				if (tmp != null)
					mt = tmp;
			}
		}
		int nb = Executor.getInstance().getActiveJobIds().contains(mt.getJobUuid()) ? 0 : (mt.getNb() + 1);
		responseObserver.onNext(MisraToken.newBuilder().setUuid(mt.getUuid()).setJobUuid(mt.getJobUuid()).setNb(nb)
				.addAllLog(mt.getLogList()).addLog(sender).build());
		responseObserver.onCompleted();
	}
}
