package cz.cvut.fel.filodami.dprt.Daemons;

import java.util.List;

import cz.cvut.fel.filodami.dprt.Executor;

public class ExecutorCacheSupplyDaemon extends Thread {

	private final int interval;
	private final int replenishThreshold;
	private Executor executor;

	public ExecutorCacheSupplyDaemon(int intervalmsec, int replenishThreshold) {
		this.interval = intervalmsec;
		this.replenishThreshold = replenishThreshold;
	}

	@Override
	public void run() {
		this.executor = Executor.getInstance();
		try {
			List<String> replenishCandidates;
			while (true) {
				replenishCandidates = executor.getReplenishCandidates(replenishThreshold);
				for (String jid : replenishCandidates) {
					executor.getClient().replenish(jid);
				}
				Thread.sleep(interval);
			}
		} catch (InterruptedException e) {
		}
	}
}
