package cz.cvut.fel.filodami.dprt.Daemons;

import cz.cvut.fel.filodami.dprt.Executor;

public class JobDiscoveryDaemon extends Thread {

	private final int interval;
	private Executor executor;

	public JobDiscoveryDaemon(int intervalmsec) {
		this.interval = intervalmsec;
	}

	@Override
	public void run() {
		this.executor = Executor.getInstance();
		try {
			while (true) {
				executor.getClient().discover();
				Thread.sleep(interval);
			}
		} catch (InterruptedException e) {
		}
	}
}
