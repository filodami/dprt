package cz.cvut.fel.filodami.dprt.Solvers;

import cz.cvut.fel.filodami.dprt.Chunk;
import cz.cvut.fel.filodami.dprt.Dictionary;
import cz.cvut.fel.filodami.dprt.Executor;
import cz.cvut.fel.filodami.dprt.Job;

public abstract class Solver implements Runnable {
	protected final String ciphertext;
	private final String alphabet;
	private final Dictionary dict;
	private final int maxPos;
	private final Chunk chunk;

	public Solver(Job job, Dictionary dict, Chunk chunk) {
		this.ciphertext = job.getCiphertext();
		this.alphabet = job.getAlphabet();
		this.dict = dict;
		this.maxPos = chunk.getFromList().size() - 1;
		this.chunk = chunk;
	}

	private void solve(String base, int saltLength, StringBuilder saltBuilder) throws SolutionFoundException, InterruptedException {
		for (int i = chunk.getFromList().get(saltLength); i <= chunk.getToList().get(saltLength); i++) {
			saltBuilder.append(alphabet.charAt(i));
			if (hashAndCompare(base + saltBuilder.toString()))
				throw new SolutionFoundException(base + saltBuilder.toString());
			if (hashAndCompare(saltBuilder.toString() + base))
				throw new SolutionFoundException(saltBuilder.toString() + base);
			if (saltLength < maxPos) {
				if (Thread.interrupted())
					throw new InterruptedException();
				solve(base, saltLength + 1, saltBuilder);
			}
				
			saltBuilder.deleteCharAt(saltLength);
		}
	}

	@Override
	public void run() {
		try {
			final int dictLen = dict.getLength();
			for (int i = 0; i < dictLen; i++) {
				solve(dict.getLine(i), 0, new StringBuilder(chunk.getFromList().size()));
			}
		} catch (SolutionFoundException e) {
			Executor.getInstance().submitSolution(chunk.getJobUuid(), e.getSolution());
			return;
		} catch (InterruptedException e) {
			return;
		}
		Executor.getInstance().finishChunk(chunk);

	}

	protected abstract boolean hashAndCompare(String toHash);

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Solver [Job=");
		builder.append(chunk.getJobUuid());
		builder.append(", from=");
		builder.append(chunk.getFromList());
		builder.append(", to=");
		builder.append(chunk.getToList());
		builder.append("]");
		return builder.toString();
	}

	
	
	
}
