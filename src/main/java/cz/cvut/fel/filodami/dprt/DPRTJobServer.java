package cz.cvut.fel.filodami.dprt;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import cz.cvut.fel.filodami.dprt.DPRTJobsGrpc.DPRTJobsImplBase;
import io.grpc.stub.StreamObserver;

public class DPRTJobServer extends DPRTJobsImplBase {

	@Override
	public void discoverJobs(TextMessage request, StreamObserver<Job> responseObserver) {
		Collection<Job> jobs = Executor.getInstance().getActiveJobs();
		for (Job job : jobs) {
			responseObserver.onNext(job);
		}
		responseObserver.onCompleted();
	}

	@Override
	public void pushJob(Job request, StreamObserver<TextMessage> responseObserver) {
		boolean contains = Executor.getInstance().registerJob(request);
		responseObserver.onNext(
				TextMessage.newBuilder().setText(contains ? "Already present." : "Not present, registered.").build());
		responseObserver.onCompleted();
		if (!contains) {
			Executor.getInstance().getClient().push(request);
		}
	}

	@Override
	public void requestChunk(UUIDMessage request, StreamObserver<Chunk> responseObserver) {
		Chunk c = null;
		try {
			c = Executor.getInstance().requestChunk(request.getUuid(), App.getConfig().getEccSize());
		} catch (AlreadyKilledException e) {
			Executor.getInstance().getClient().kill(request.getUuid());
		}
		if (c != null)
			responseObserver.onNext(c);	
		responseObserver.onCompleted();
	}

	@Override
	public void pushResult(Result request, StreamObserver<TextMessage> responseObserver) {
		boolean nowKilled = Executor.getInstance().submitSolution(request.getJobUuid(), request.getResult());
		responseObserver.onNext(
				TextMessage.newBuilder().setText(nowKilled ? "Not killed, killing." : "Already killed.").build());
		responseObserver.onCompleted();
	}

	@Override
	public void killJob(UUIDMessage request, StreamObserver<TextMessage> responseObserver) {
		StringBuilder sb = new StringBuilder();
		List<String> killedNow = new ArrayList<>();
			boolean nowKilled = Executor.getInstance().killJob(request.getUuid(), false);
			sb.append(request.getUuid());
			if (nowKilled) {
				sb.append(" : Not killed, killing.\n");
				killedNow.add(request.getUuid());
			} else
				sb.append(" : Already killed.\n");
		responseObserver.onNext(TextMessage.newBuilder().setText(sb.toString()).build());
		responseObserver.onCompleted();
		for (String string : killedNow) {
			Executor.getInstance().getClient().kill(string);
		}
	}

	@Override
	public void resetAll(TextMessage request, StreamObserver<TextMessage> responseObserver) {
		if (App.getState() != AppState.CONFIG)
			Executor.getInstance().getClient().reset();
		App.resetNode();
	}
}
