package cz.cvut.fel.filodami.dprt.Daemons;

import cz.cvut.fel.filodami.dprt.Executor;
import cz.cvut.fel.filodami.dprt.Overwatch;

public class ConnectionManagementDaemon extends Thread {
	private final int interval;
	private Executor executor;
	private Overwatch overwatch;

	public ConnectionManagementDaemon(int intervalmsec) {
		this.interval = intervalmsec;
	}

	@Override
	public void run() {
		this.executor = Executor.getInstance();
		this.overwatch = Overwatch.getInstance();
		try {
			while (true) {
				executor.getClient().refreshChannels();
				overwatch.getClient().refreshChannels();
				Thread.sleep(interval);
			}
		} catch (InterruptedException e) {
		}
	}
}
