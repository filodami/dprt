package cz.cvut.fel.filodami.dprt;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JobBuilder {
	private static final List<String> ALLOWED_ALGORITHMS = Arrays.asList(new String[] { "MD5", "SHA1" });
	private static final Pattern MD5_REGEX = Pattern.compile("^[a-f0-9]{32}$");
	private static final Pattern SHA1_REGEX = Pattern.compile("\\b[0-9a-f]{5,40}\\b");

	private String algorithm;
	private String alphabet;
	private String ciphertext;
	private String dictionary;
	private int saltSize;

	public JobBuilder(String algorithm, String alphabet, String ciphertext, String dictionary, int saltSize) {
		super();
		this.algorithm = algorithm;
		this.alphabet = alphabet;
		this.ciphertext = ciphertext;
		this.dictionary = dictionary;
		this.saltSize = saltSize;
	}

	private static boolean validateCiphertext(String ciphertext, String algorithm) {
		Matcher m;
		switch (algorithm) {
		case "MD5":
			m = MD5_REGEX.matcher(ciphertext);
			return m.matches();
		case "SHA1":
			m = SHA1_REGEX.matcher(ciphertext);
			return m.matches();
		default:
			return false;
		}
	}

	public Job build() {
		return Job.newBuilder().setAlgorithm(algorithm).setAlphabet(alphabet).setCiphertext(ciphertext)
				.setDictionaryName(dictionary).setSaltSize(saltSize).setUuid(UUID.randomUUID().toString()).build();
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public void setAlgorithm(String algorithm) throws InvalidParameterException {
		if (!ALLOWED_ALGORITHMS.contains(algorithm))
			throw new InvalidParameterException("No algorithm available with name: \"" + algorithm + "\"");
		this.algorithm = algorithm;
		this.ciphertext = "";
	}

	public String getAlphabet() {
		return alphabet;
	}

	public void setAlphabet(String alphabet) throws InvalidParameterException {
		if (alphabet.isEmpty())
			throw new InvalidParameterException("Alphabet must contain at least one character.");
		this.alphabet = alphabet;
	}

	public String getCiphertext() {
		return ciphertext;
	}

	public void setCiphertext(String ciphertext) throws InvalidParameterException {
		if (!validateCiphertext(ciphertext, this.algorithm))
			throw new InvalidParameterException("The ciphertext provided is not compatible with the algorithm selected.");
		this.ciphertext = ciphertext.toUpperCase();
	}

	public String getDictionary() {
		return dictionary;
	}

	public void setDictionary(String dictionary) throws InvalidParameterException {
		if (!App.getConfig().getDictionaries().contains(dictionary))
			throw new InvalidParameterException("No dictionary available with name: \"" + dictionary + "\"");
		this.dictionary = dictionary;
	}

	public int getSaltSize() {
		return saltSize;
	}

	public void setSaltSize(int saltSize) throws InvalidParameterException {
		if (saltSize < 0)
			throw new InvalidParameterException("Salt size less than zero.");
		this.saltSize = saltSize;
	}

	public void setSaltSize(String saltSize) throws InvalidParameterException {
		try {
			setSaltSize(Integer.parseInt(saltSize));
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Unable to parse int.");
		}
	}
	
	public boolean isValid() {
		return !ciphertext.isEmpty() && !dictionary.isEmpty();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nParameters set:\n  Algorithm: ");
		builder.append(algorithm);
		builder.append("\n  Alphabet: ");
		builder.append(alphabet);
		builder.append("\n  Salt size: ");
		builder.append(saltSize);
		builder.append("\n  Dictionary: ");
		builder.append(dictionary);
		builder.append("\n  Ciphertext: ");
		builder.append(ciphertext);
		builder.append("\n");
		return builder.toString();
	}

}
