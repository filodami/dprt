package cz.cvut.fel.filodami.dprt.Solvers;

public class SolutionFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public SolutionFoundException(String solution) {
		super(solution);
	}
	
	public String getSolution() {
		return getMessage();
	}
}
