package cz.cvut.fel.filodami.dprt;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.cvut.fel.filodami.dprt.DPRTJobsGrpc.DPRTJobsBlockingStub;
import cz.cvut.fel.filodami.dprt.DPRTJobsGrpc.DPRTJobsStub;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

public class DPRTJobClient {
	private final Logger log = LoggerFactory.getLogger(DPRTJobClient.class);
	private final Map<String, ManagedChannel> neighbors = new HashMap<>();

	public DPRTJobClient() {
		for (String neighbor : App.getConfig().getNeighbors()) {
			neighbors.put(neighbor, ManagedChannelBuilder.forTarget(neighbor).usePlaintext().build());
		}
	}

	public void replenish(String toReplenish) {
		log.info("Replenish request for Jobs: " + toReplenish);
		if (toReplenish.isEmpty())
			return;
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsBlockingStub stub = DPRTJobsGrpc.newBlockingStub(channel);
				Iterator<Chunk> chunk = null;
				try {
					chunk = stub.requestChunk(UUIDMessage.newBuilder().setUuid(toReplenish).build());
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
				if (chunk != null && chunk.hasNext()) {
					while (chunk.hasNext()) {
						Executor.getInstance().registerChunk(chunk.next());
					}
					log.info("Job " + toReplenish + " replenished from " + channel.authority());
				}
			}
		}
	}

	public void discover() {
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsBlockingStub stub = DPRTJobsGrpc.newBlockingStub(channel);
				Iterator<Job> jobs = null;
				try {
					jobs = stub.discoverJobs(TextMessage.newBuilder().setText(App.getConfig().getNodeName()).build());
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
				if (jobs != null && jobs.hasNext()) {
					while (jobs.hasNext()) {
						Executor.getInstance().registerJob(jobs.next());
					}
					log.info("Discovered Jobs from " + channel.authority());
				}
			}
		}
	}

	public void kill(String toKill) {
		log.info("Kill request for Job: " + toKill);
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsBlockingStub stub = DPRTJobsGrpc.newBlockingStub(channel);
				try {
					TextMessage response = stub.killJob(UUIDMessage.newBuilder().setUuid(toKill).build());
					log.info("Kill response from " + channel.authority() + " is: " + response.getText());
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
			}
		}
	}

	public void push(Job toPush) {
		log.info("Job push for Job: " + toPush.getUuid());
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsBlockingStub stub = DPRTJobsGrpc.newBlockingStub(channel);
				try {
					TextMessage response = stub.pushJob(toPush);
					log.info("Job " + toPush.getUuid() + " pushed to " + channel.authority() + ". And the response is: "
							+ response.getText());
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
			}
		}
	}

	public void heureka(Result result) {
		log.info("Heureka for Job: " + result.getJobUuid());
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsStub stub = DPRTJobsGrpc.newStub(channel);
				try {
					stub.pushResult(result, new StreamObserver<TextMessage>() {
						
						@Override
						public void onNext(TextMessage value) {
							log.info(
									"Result pushed to " + channel.authority() + ". And the response is: " + value.getText());
						}
						
						@Override
						public void onError(Throwable t) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void onCompleted() {
							// TODO Auto-generated method stub
							
						}
					});
					
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
			}
		}
	}

	public void reset() {
		log.info("Reset request.");
		for (ManagedChannel channel : neighbors.values()) {
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTJobsBlockingStub stub = DPRTJobsGrpc.newBlockingStub(channel);
				try {
					TextMessage response = stub
							.resetAll(TextMessage.newBuilder().setText(App.getConfig().getNodeName()).build());
					log.info("Reset pushed to " + channel.authority() + ". And the response is: " + response.getText());
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
			}
		}
	}

	public void shutdown() {
		for (ManagedChannel channel : neighbors.values()) {
			channel.shutdownNow();
		}
	}
	
	public void refreshChannels() {
		for (ManagedChannel channel : neighbors.values()) {
			channel.getState(true);
		}
	}
}
