package cz.cvut.fel.filodami.dprt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Config {
	public static final Pattern IPv4_REGEX = Pattern
			.compile("^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\\.(?!$)|$)){4}$");
	public static final Pattern PORT_REGEX = Pattern.compile(
			"^((6553[0-5])|(655[0-2][0-9])|(65[0-4][0-9]{2})|(6[0-4][0-9]{3})|([1-5][0-9]{4})|([0-5]{0,5})|([0-9]{1,4}))$");

	private String nodeName = UUID.randomUUID().toString();
	private List<String> neighbors = new ArrayList<>();
	private int threads = 1;
	private Set<String> dictionaries = new HashSet<>();
	private int ecsInterval = 30000;
	private int eccSize = 3;

	public static Config defaultConfig() {
		return new Config();
	}

	public void addNeighbor(String ipv4address, String port) throws InvalidParameterException {
		Matcher mi = IPv4_REGEX.matcher(ipv4address);
		Matcher mp = PORT_REGEX.matcher(port);
		if (!mi.matches())
			throw new InvalidParameterException("Not a valid IPv4.");
		if (mp.matches())
			neighbors.add(ipv4address + ":" + port);
		else
			throw new InvalidParameterException("Not a valid port number.");
	}

	public void removeNeighbor(String index) throws InvalidParameterException {
		try {
			removeNeighbor(Integer.parseInt(index));
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Cannot parse integer.");
		}
	}

	public void removeNeighbor(int index) throws InvalidParameterException {
		try {
			neighbors.remove(index);
		} catch (IndexOutOfBoundsException e) {
			throw new InvalidParameterException("Neighbor with id " + index + " not present.");
		}
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public List<String> getNeighbors() {
		return neighbors;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(String threads) throws InvalidParameterException {
		try {
			setThreads(Integer.parseInt(threads));
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Cannot parse integer.");
		}
	}

	public void setThreads(int threads) throws InvalidParameterException {
		if (threads < 1)
			throw new InvalidParameterException("Cannot set less than 1 thead.");
		this.threads = threads;
	}

	public int getEcsInterval() {
		return ecsInterval;
	}

	public void setEcsInterval(String ecsInterval) throws InvalidParameterException {
		try {
			setEcsInterval(Integer.parseInt(ecsInterval));
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Cannot parse integer.");
		}
	}

	public void setEcsInterval(int ecsInterval) throws InvalidParameterException {
		if (ecsInterval < 1000)
			throw new InvalidParameterException("Cannot set less than 1 second.");
		this.ecsInterval = ecsInterval;
	}

	public int getEccSize() {
		return eccSize;
	}

	public void setEccSize(String eccSize) throws InvalidParameterException {
		try {
			setEccSize(Integer.parseInt(eccSize));
		} catch (NumberFormatException e) {
			throw new InvalidParameterException("Cannot parse integer.");
		}
	}

	public void setEccSize(int eccSize) throws InvalidParameterException {
		if (ecsInterval < 1)
			throw new InvalidParameterException("The cache cannot be smaller than 1 chunk.");
		this.eccSize = eccSize;
	}

	public Set<String> getDictionaries() {
		return dictionaries;
	}

	public void addDictionary(String dictionary) throws InvalidParameterException {
		if (new File(dictionary + ".txt").exists())
			dictionaries.add(dictionary);
		else
			throw new InvalidParameterException("The dictionary file with name \"" + dictionary + "\" does not exist.");
	}

	public void removeDictionary(String dictionary) {
		dictionaries.remove(dictionary);
	}
	
	public static void validateAddress(String ipv4address, String port) throws InvalidParameterException {
		Matcher mi = IPv4_REGEX.matcher(ipv4address);
		Matcher mp = PORT_REGEX.matcher(port);
		if (!mi.matches())
			throw new InvalidParameterException("Not a valid IPv4.");
		if (!mp.matches())
			throw new InvalidParameterException("Not a valid port number.");
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("\nConfig:\n Node Name: ");
		builder.append(nodeName);
		builder.append("\n Neighbors: ");
		builder.append(neighbors);
		builder.append("\n Dictionaries: ");
		builder.append(dictionaries);
		builder.append("\n Threads: ");
		builder.append(threads);
		builder.append("\n Executor Cache replenish interval (s): ");
		builder.append(ecsInterval / 1000);
		builder.append("\n Executor Chunk Cache size: ");
		builder.append(eccSize);
		builder.append("\n");
		return builder.toString();
	}
}
