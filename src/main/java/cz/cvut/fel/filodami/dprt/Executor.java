package cz.cvut.fel.filodami.dprt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.cvut.fel.filodami.dprt.Daemons.ConnectionManagementDaemon;
import cz.cvut.fel.filodami.dprt.Daemons.ExecutorCacheSupplyDaemon;
import cz.cvut.fel.filodami.dprt.Daemons.JobDiscoveryDaemon;
import cz.cvut.fel.filodami.dprt.Solvers.MD5Solver;
import cz.cvut.fel.filodami.dprt.Solvers.SolverThreadFactory;

public class Executor {
	private final static Logger log = LoggerFactory.getLogger(Executor.class);
	private static Executor instance;
	private final int THREAD_COUNT = App.getConfig().getThreads();
	private Map<String, Job> activeJobs = new HashMap<>();
	private Map<String, Job> killedJobs = new HashMap<>();
	private Map<String, Queue<Chunk>> cache = new HashMap<>();
	private Map<String, Map<Chunk, Future<?>>> execution = new HashMap<>();
	private Map<String, List<Chunk>> finished = new HashMap<>();
	private Map<String, String> results = new HashMap<>();

	private ExecutorCacheSupplyDaemon ecsd;
	private JobDiscoveryDaemon jdd;
	private ConnectionManagementDaemon cmd;
	private ExecutorService tpool = Executors.newFixedThreadPool(THREAD_COUNT, new SolverThreadFactory());
	private DPRTJobClient client = new DPRTJobClient();

	private Executor() {
		ecsd = new ExecutorCacheSupplyDaemon(App.getConfig().getEcsInterval(), App.getConfig().getEccSize());
		jdd = new JobDiscoveryDaemon(60000);
		cmd = new ConnectionManagementDaemon(5000);
	}

	public static Executor getInstance() {
		if (instance == null) {
			instance = new Executor();
			instance.ecsd.start();
			instance.jdd.start();
			instance.cmd.start();
		}
		return instance;
	}

	public static void clear() {
		if (instance != null) {
			instance.ecsd.interrupt();
			instance.jdd.interrupt();
			instance.cmd.interrupt();
			instance.tpool.shutdownNow();
			instance.client.shutdown();
			instance = null;
			log.info("Executor cleared.");
		}
	}

	public boolean registerJob(Job job) {
		boolean contains = activeJobs.containsKey(job.getUuid()) || killedJobs.containsKey(job.getUuid());
		if (!contains) {
			activeJobs.put(job.getUuid(), job);
			cache.put(job.getUuid(), new ConcurrentLinkedQueue<>());
			execution.put(job.getUuid(), new HashMap<>());
			finished.putIfAbsent(job.getUuid(), new ArrayList<>());
			log.info("Job " + job.getUuid() + " registered.");
		}
		return contains;
	}

	public void registerNewJob(Job job, int splitOrder) {
		registerJob(job);
		List<Chunk> chunks = generateChunks(job, splitOrder);
		cache.get(job.getUuid()).addAll(chunks);
		while (tryExecuteChunk(job.getUuid())) {
		}
		client.push(job);
		log.info("Job " + job.getUuid() + " created, registered and pushed.");
	}

	private List<Chunk> generateChunks(Job job, int splitOrder) {
		List<Range> rList = new ArrayList<>();
		int max = job.getAlphabet().length() - 1;
		Integer[] beginning = new Integer[job.getSaltSize()];
		Integer[] end = new Integer[job.getSaltSize()];
		for (int i = 0; i < end.length; i++) {
			beginning[i] = 0;
			end[i] = max;
		}

		rList.add(new Range(beginning, end, max));
		try {
			for (int i = 0; i < splitOrder; i++) {
				rList = Range.splitRanges(rList);
			}
		} catch (NoSplitException e) {
		}
		List<Chunk> cList = new ArrayList<>(rList.size());
		for (Range r : rList) {
			cList.add(Chunk.newBuilder().addAllFrom(Arrays.asList(r.from)).addAllTo(Arrays.asList(r.to))
					.setJobUuid(job.getUuid()).build());
		}
		return cList;
	}

	public void registerChunk(Chunk chunk) {
		if (activeJobs.containsKey(chunk.getJobUuid())) {
			cache.get(chunk.getJobUuid()).add(chunk);
			tryExecuteChunk(chunk.getJobUuid());
		}
		log.info("Chunk for Job " + chunk.getJobUuid() + " from:" + chunk.getFromList() + " to:" + chunk.getToList()
				+ " registered.");
	}

	public boolean tryExecuteChunk(String jid) {
		boolean ret = false;
		if (activeJobs.containsKey(jid) && execution.get(jid).size() < (THREAD_COUNT / activeJobs.size()) + 1
				&& cache.get(jid).size() > 0) {
			Chunk exec = cache.get(jid).poll();
			Job exej = activeJobs.get(jid);
			Dictionary exed = App.getDictionaries().get(exej.getDictionaryName());
			Future<?> f = null;
			switch (exej.getAlgorithm()) {
			case "MD5":
				f = tpool.submit(new MD5Solver(exej, exed, exec));
				ret = true;
				break;
			default:
				// TODO Algorithm not supported
			}
			execution.get(jid).put(exec, f);
			log.debug("Chunk for Job " + exec.getJobUuid() + " from:" + exec.getFromList() + " to:" + exec.getToList()
					+ " queued for execution.");
		}
		return ret;
	}

	public List<String> getReplenishCandidates(int threshold) {
		ArrayList<String> list = new ArrayList<>();
		cache.forEach(new BiConsumer<String, Queue<Chunk>>() {

			@Override
			public void accept(String t, Queue<Chunk> u) {
				if (u.size() < threshold)
					list.add(t);
			}
		});
		return list;
	}

	public Collection<Job> getActiveJobs() {
		return activeJobs.values();
	}
	
	public Set<String> getActiveJobIds() {
		return activeJobs.keySet();
	}

	public Chunk requestChunk(String jid, int threshold) throws AlreadyKilledException {
		Queue<Chunk> q = cache.get(jid);
		if (q != null) {
			if (q.size() >= threshold)
				return q.poll();
		} else {
			if (killedJobs.containsKey(jid))
				throw new AlreadyKilledException();
		}
		return null;
	}

	public boolean killJob(String jid, boolean graceful) {
		boolean kill = false;
		if (activeJobs.containsKey(jid))
			kill = true;
		if (kill) {
			killedJobs.put(jid, activeJobs.remove(jid));
			cache.remove(jid);
			for (Future<?> f : execution.get(jid).values()) {
				f.cancel(true);
			}
			execution.remove(jid);
			log.warn("Job " + jid + " killed.");
		}
		return kill;
	}

	public boolean submitSolution(String jobUuid, String solution) {
		boolean nowKilled = killJob(jobUuid, true);
		if (nowKilled)
			client.heureka(Result.newBuilder().setJobUuid(jobUuid).setResult(solution).build());
		results.putIfAbsent(jobUuid, solution);
		log.info("Solution for Job " + jobUuid + " found!");
		return nowKilled;
	}

	public void finishChunk(Chunk chunk) {
		execution.get(chunk.getJobUuid()).remove(chunk);
		finished.get(chunk.getJobUuid()).add(chunk);
		tryExecuteChunk(chunk.getJobUuid());
		log.debug("Chunk for Job " + chunk.getJobUuid() + " from:" + chunk.getFromList() + " to:" + chunk.getToList()
				+ " finished execution.");
	}

	public DPRTJobClient getClient() {
		return client;
	}

	public Collection<Job> getKilledJobs() {
		return killedJobs.values();
	}

	public Map<String, String> getResults() {
		return results;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Executor [THREAD_COUNT=");
		builder.append(THREAD_COUNT);
		builder.append(", activeJobs=");
		builder.append(activeJobs);
		builder.append(", killedJobs=");
		builder.append(killedJobs);
		builder.append(", cache=");
		builder.append(cache);
		builder.append(", execution=");
		builder.append(execution);
		builder.append(", finished=");
		builder.append(finished);
		builder.append(", ecsd=");
		builder.append(ecsd);
		builder.append(", tpool=");
		builder.append(tpool);
		builder.append(", client=");
		builder.append(client);
		builder.append("]");
		return builder.toString();
	}

}
