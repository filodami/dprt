package cz.cvut.fel.filodami.dprt.Solvers;

import java.util.concurrent.ThreadFactory;

public class SolverThreadFactory implements ThreadFactory {

	@Override
	public Thread newThread(Runnable r) {
		Thread t = new Thread(r);
		try {
			Solver s = (Solver) r;
			t.setName(s.toString());
		} catch (ClassCastException e) {
		}
		return t;
	}

}
