package cz.cvut.fel.filodami.dprt;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.cvut.fel.filodami.dprt.DPRTManagerGrpc.DPRTManagerBlockingStub;
import io.grpc.ConnectivityState;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

public class DPRTManagerClient {
	private final Logger log = LoggerFactory.getLogger(DPRTManagerClient.class);
	private final Map<String, ManagedChannel> neighbors = new HashMap<>();

	public DPRTManagerClient() {
		for (String neighbor : App.getConfig().getNeighbors()) {
			neighbors.put(neighbor, ManagedChannelBuilder.forTarget(neighbor).usePlaintext().build());
		}
	}

	public MisraToken sendToken(String to, MisraToken mt) {
		ManagedChannel channel = neighbors.get(to);
		ConnectivityState cstate = channel.getState(false);
		if (cstate == ConnectivityState.READY) {
			DPRTManagerBlockingStub stub = DPRTManagerGrpc.newBlockingStub(channel);
			try {
				MisraToken response = stub.submitTD(mt);
				log.info("Submitted token for Job " + mt.getJobUuid() + ", to " + channel.authority());
				return response;
			} catch (StatusRuntimeException e) {
				log.warn("RPC failed: " + e.getStatus());
			}
		}
		return null;
	}

	public MisraToken sendFirstToken(String jid, String thisAddress) {
		MisraToken response = null;
		
		for (ManagedChannel channel : neighbors.values()) {
			if (response != null && response.getLogList().contains(channel.authority()))
				continue;
			ConnectivityState cstate = channel.getState(false);
			if (cstate == ConnectivityState.READY) {
				DPRTManagerBlockingStub stub = DPRTManagerGrpc.newBlockingStub(channel);
				try {
					if (response == null) {
						MisraToken firstmt = MisraToken.newBuilder().setUuid(UUID.randomUUID().toString()).setJobUuid(jid)
								.setNb(2).addLog(thisAddress).addLog(channel.authority()).build();
						Overwatch.getInstance().wasVisited(firstmt.getUuid());
						response = stub.submitTD(firstmt);
						log.info("Submitted first token for Job " + firstmt.getJobUuid() + ", to " + channel.authority());
					} else {
						int nb = Executor.getInstance().getActiveJobIds().contains(response.getJobUuid()) ? 0 : (response.getNb() + 1);
						MisraToken tmp = Overwatch.getInstance().getClient().sendToken(channel.authority(),
								MisraToken.newBuilder().setUuid(response.getUuid()).setJobUuid(response.getJobUuid()).setNb(nb)
										.addAllLog(response.getLogList()).addLog(channel.authority()).build());
						if (tmp != null)
							response = tmp;
					}
				} catch (StatusRuntimeException e) {
					log.warn("RPC failed: " + e.getStatus());
				}
			}
		}
		if (response != null)
			log.info("MisraToken Log: " + response.getLogList() + ", NB: " + response.getNb());
		return response;
	}

	public void shutdown() {
		for (ManagedChannel channel : neighbors.values()) {
			channel.shutdownNow();
		}
	}

	public void refreshChannels() {
		for (ManagedChannel channel : neighbors.values()) {
			channel.getState(true);
		}
	}

	public Set<String> getNeighbors() {
		return neighbors.keySet();
	}

}
