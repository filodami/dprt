package cz.cvut.fel.filodami.dprt.Solvers;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import cz.cvut.fel.filodami.dprt.Chunk;
import cz.cvut.fel.filodami.dprt.Dictionary;
import cz.cvut.fel.filodami.dprt.Job;

public class MD5Solver extends Solver {
	private MessageDigest md;

	public MD5Solver(Job job, Dictionary dict, Chunk chunk) {
		super(job, dict, chunk);
		try {
			this.md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected boolean hashAndCompare(String toHash) {
		md.update(toHash.getBytes());
		byte[] digest = md.digest();
		md.reset();
		String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
		return ciphertext.equals(myHash);
	}

}
