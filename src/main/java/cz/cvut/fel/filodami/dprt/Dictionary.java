package cz.cvut.fel.filodami.dprt;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Dictionary {
	private List<String> dict;
	private String name;

	public Dictionary(List<String> dict, String name) {
		this.dict = dict;
		this.name = name;
	}
	public static Dictionary loadDictionary(String dictionary) throws IOException {
		return loadDictionary(new File(dictionary + ".txt"));
	}
	public static Dictionary loadDictionary(File dictionary) throws IOException {
		FileReader fr = null;
		BufferedReader br = null;
		String name;
		List<String> dict;
		
		try {
			fr = new FileReader(dictionary);
			br = new BufferedReader(fr);
			name = br.readLine();
			int len = 1000;
			try {
				len = Integer.parseInt(br.readLine());
			} catch (NumberFormatException e) {
			}
			dict = new ArrayList<>(len + 1);
			String s;
			while ((s = br.readLine()) != null) {
				dict.add(s);
			}
		} finally {
			if (br != null) {
				br.close();
			}
			if (fr != null) {
				fr.close();
			}
		}
		if (name != null && dict != null)
			return new Dictionary(dict, name);
		else
			return null;
	}
	public String getLine(int index) {
		return dict.get(index);
	}
	public int getLength() {
		return dict.size();
	}
	public String getName() {
		return name;
	}
}
