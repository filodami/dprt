package cz.cvut.fel.filodami.dprt;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Overwatch {
	private final static Logger log = LoggerFactory.getLogger(Overwatch.class);
	private static Overwatch instance;
	private DPRTManagerClient client = new DPRTManagerClient();
	private List<String> visitedBy = new ArrayList<>();

	private Overwatch() {

	}

	public static Overwatch getInstance() {
		if (instance == null) {
			instance = new Overwatch();
		}
		return instance;
	}

	public static void clear() {
		if (instance != null) {
			instance.client.shutdown();
			instance = null;
			log.info("Overwatch cleared.");
		}
	}

	public boolean isTerminated(String jid, String thisAddress) {
		MisraToken mt = client.sendFirstToken(jid, thisAddress);
		if (mt != null)
			return mt.getLogCount() == mt.getNb();
		return true;
	}

	public boolean wasVisited(String tdid) {
		if (!visitedBy.contains(tdid)) {
			visitedBy.add(tdid);
			return false;
		}
		return true;
	}

	public DPRTManagerClient getClient() {
		return client;
	}

}
